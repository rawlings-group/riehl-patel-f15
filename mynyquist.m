function mynyquist(varargin)
%
% 
%   mynyquist(sys1,sys2,...)
%   mynyquist(sys1,sys2,..., opts)
%   mynyquist(sys1,sys2,...,wout,opts) 
%   mynyquist(sys1,sys2,...,wout)
%    
%   Where sys is the system input, wout is the output frequency range, and opts 
%   is a modifiable option set 
%    
%   Nyquist diagram generator with plot modification capabilities offered in 
%   MATLAB.
%
%   sys can be a matrix of or single transfer function inputs  
% 
%   Relies on an option structure from mynyquistoptions. See mynyquistoptions for
%   documentation on the various options.
%
%   The three variables consistent in this program are the Imginary(Im or im),
%   Real (Re or re), and the output frequency (wout).
% 
%   For SISO system with default options, the normal nyquist function is 
%   recommended.
% 
%   Example:
%   sys = tf(1, [1 1]);
%   opts = mynyquistoptions;
%   % Changes title color from default black to green   
%   opts.Title.Color = [0 1 0];
%   mynyquist(sys, opts) 

% -------------------------------------------------------------------------
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------



% Creates an initial figure for the plot(s)
figure
%Creates a color set for having different systems having different plot colors
ycolors=get(gca,'colororder');
%Creates a standard output frequency range
wout=linspace(1e-2,100);
%Check to see what input type was specified
%if an option set is specified
if isstruct(varargin{end})
   opts  =  varargin{end};
   %if a frequency set is specified
   if isa(varargin{end-1},'numeric')
      wout  =  varargin{end-1};
      %gets the number of system inputs
      qqq=nargin-2;
      %rest of inputs are the transfer fuction systems
       aa = varargin(1:(nargin-2));
   else %if no output frequency range is given
      %gets the number of system inputs
      qqq=nargin-1;
      %rest of inputs are the transfer fuction systems
      aa = varargin(1:(nargin-1));
   end
% if no option set is inputted but a output frequency range is
elseif isa(varargin{end},'numeric')
%  obtains the standard bode option set
   opts=mynyquistoptions;
   wout = varargin{end};
   %get the number of system inputs
   qqq=nargin-1;
   %rest of inputs are the transfer fuction systems
   aa = varargin(1:(nargin-1));
 
else %If the input does not specify an option set or an output frequency
%  obtains the standard bode option set
  opts=mynyquistoptions;
  %get the number of system inputs
  qqq=nargin;
  %rest of inputs are the transfer fuction systems
  aa = varargin(1:(nargin));
end

%resizes the size of the system inputs according to visibility specifications
for iii = 1:length(aa)  
       %gets size of given system input
       y  =  ones(size(aa{iii}));
       [mmm(iii),nnn(iii)]  =  size(y);
       %obtains the max size of the system inputs
       aaaaa=max(mmm);
       bbbbb=max(nnn);      
       %function resizes the size matrix if specified
       x=resize_nyquist(y);
       [mm(iii),nn(iii)]  =  size(x);
       %obtains new max size of the system inputs after resizing 
       as=max(mm);
       bs=max(nn);
end 
%sets initial value for flag for proper evaluation
flag=0;
%creates a number system for the each system input for labeling purposes
zzz=linspace(1,qqq,length(aa));

for iiii = 1:length(aa)
    %obtains which number the input is
    abc=iiii;
    %obtains given system input
    aaa=aa{iiii};
    %gets appropriate linecolor for system input
    linecolor=ycolors(iiii,:);
    %check to see if the given system is the max size system for labeling purposes
    if isequal([mmm(iiii) nnn(iiii)],[aaaaa bbbbb])
       flag=1;
    elseif flag==1
       flag=0;
    else
       flag=0;
    end  
    if iiii==length(aa)
       flag1=1;
    else
       flag1=0;
    end
    %nyquist function that creates the plots
    nyquist_figure(aaa,opts,as,bs,flag,linecolor,wout,flag1,abc,zzz(iiii))
    hold on
end

%Function to resize the size matrix for the subplot size specifications
function x=resize_nyquist(y)

       [m,n]  =  size(y);
       %Makes an output cell array of the correct size for rest of function
       if strcmpi(opts.InputVisible,'on')
          opts.InputVisible  =  {};
          for i = 1:n
              opts.InputVisible(i) = {'on'};
          end
       else
       end
       %Makes an input cell array of the correct size for rest of function
       if strcmpi(opts.OutputVisible,'on')
          opts.OutputVisible = {};
          for ii = 1:m
              opts.OutputVisible(ii) = {'on'};
          end
       else
       end
       %these two loops changes the size of the size matrix if specified
       for j = 1:n
           if strcmpi(opts.InputVisible(j),'off')
              y(:,j)=[];    
           else
           end
       end
       
       for k = 1:m
           if strcmpi(opts.OutputVisible(k),'off')
              y(k,:)=[];
           else
           end
       end
%sets the new size of the size matrix
x=y;
       
end


function [] = nyquist_figure(sys_input, opts, maxrows, maxcols, flag, linecolor,wout,flag1,abc,zzzz)
%Size fucntion used to Obtain Size of transfer function matrix to create 
%coordinate system for subplots
a=maxrows;
b=maxcols;
%gets size of current system being plotted
y  =  ones(size(sys_input));
[m,n]  =  size(y);     
%Makes an input cell array of the correct size for rest of function    
if strcmpi(opts.InputVisible,'on')
    opts.InputVisible  =  {};
    for i = 1:n
      opts.InputVisible(i) = {'on'};
    end
else
end
%Makes an output cell array of the correct size for rest of function 
if strcmpi(opts.OutputVisible,'on')
    opts.OutputVisible = {};
    for ii = 1:m
      opts.OutputVisible(ii) = {'on'};
    end
else
end

%Used to obtain values for Bode plot fo each input/output pairing
for k = 1:m 
  for j = 1:n
%     Alters length of X-axis to comply with the set XLim
     if strcmpi(opts.XLimMode,'manual')
        [re,im] = nyquist(sys_input(k,j),opts.XLim);
         wout = opts.XLim;
         for i = 1:length(re)
             Re(k,j,i) = re(i);    
         end
         for h = 1:length(im)
             Im(k,j,h) = im(h);    
         end
     else   
        [re,im,wout] = nyquist(sys_input(k,j));
         for i = 1:length(re)
             Re(k,j,i) = re(i);    
         end
         for h = 1:length(im)
             Im(k,j,h) = im(h);    
         end
     end
   end
end
%Removes the specified inputs from the bode function data
for j = 1:n
    if strcmpi(opts.InputVisible(j),'off')
       Im(:,j,:) = [];
       Re(:,j,:) = [];      
    else
    end
end
%Removes the specified outputs from the bode function data
for k = 1:m
    if strcmpi(opts.OutputVisible(k),'off')
       Im(k,:,:) = [];
       Re(k,:,:) = [];  
    else
    end
end

%Obtains the new size of the transfer function after possible deletion of 
%inputs/outputs from the transfer function
y = ones((size(Im(:,:,1))));
[m,n] = size(y);

%Turns the cell input for the specified YLim's and turns them into matrix 
%inputs
if strcmpi(opts.YLimMode,'manual')
    for i = 1:length(opts.YLim)
      YLim(:,:,i) = cell2mat(opts.YLim(i));
    end
else
end
%The following four for loops obtain size matrices for coordinates of the plots being used
for k = 1:m
  if k == 1
     c(k) = 1;
  else
     c(k) = c(k-1)+1;
  end
end
for k = 1:m 
  for j = 1:n
      if j == 1 && k == 1
         r(k,j) = y(k,j);
         p = r(k,j);
      elseif j == 1 
         r(k,j) = p+1;
         p = r(k,j);
      else
         r(k,j) = p+1;
         p = r(k,j);
      end
  end
end
for k = 1:2*m 
  for j = 1:n
      if j == 1 && k == 1
         q(k,j) = y(k,j);
         p = q(k,j);
      else
         q(k,j) = p+1;
         p = q(k,j);
      end
  end
end
for k = 1:m 
  for j = 1:n
      if j == 1 && k == 1
         s(k,j) = y(k,j);
         p = s(k,j);
      else
         s(k,j) = p+1;
         p = s(k,j);
      end
  end
end


%Where the nyquist plots of SISO systems begins
if m==1 && n==1
    if strcmpi(opts.IOGrouping, 'none')
      subplot(a,b,r(k,j))
       %Obtains the imaginary and real outputs of the nyquist function for the system
       X=Re(:);
       Y=Im(:);
       %Plots the imaginary and real values in a nyquist plot format
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));
        end
        %obtains the current axis for labeling 
        if a==1 && b==1
           axax=gca;
        end         
        
        grid_options_set
        
   %specifies if the system inputs are grouped together in one plot, for MIMO system purposes
    elseif strcmpi(opts.IOGrouping, 'inputs')
      subplot(a,1,c(k))
       %Obtains the imaginary and real outputs of the nyquist function for the system
       X=Re(:);
       Y=Im(:);
       %Plots the imaginary and real values in a nyquist plot format      
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));
        end
        %obtains the current axis for labeling        
        if k==1 && j==1
           axax=gca;
        end  
        
        grid_options_set
        
        %obtains the current axis for labeling        
        if k==a && j==b
           axaxx=hAx1;
        end 
        
        
    elseif strcmpi(opts.IOGrouping, 'outputs')  
      subplot(1,b,j)
       %Obtains the imaginary and real outputs of the nyquist function for the system
       X=Re(:);
       Y=Im(:);
       %Plots the imaginary and real values in a nyquist plot format
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));
        end
        
        %obtains the current axis for labeling
        if k==1 && j==1
           axax=gca;
        end         
        
        grid_options_set
        
        %obtains the current axis for labeling
        if k==a && j==b
           axaxx=hAx1;
        end 
        
        
    else %IOGrouping is 'all'
      subplot(1,1,1)
       %Obtains the imaginary and real outputs of the nyquist function for the system
       X=Re(:);
       Y=Im(:);
       %Plots the imaginary and real values in a nyquist plot format
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));
        end
        
        %obtains the current axis for labeling
        if k==1 && j==1
           axax=gca;
        end 
        
        grid_options_set
        
 
        %obtains the current axis for labeling
        if k==a && j==b
           axaxx=hAx1
        end 
        
        
    end

        
else
%MIMO System Plotting Begins Here
for k = 1:m 
  for j = 1:n
    if strcmpi(opts.IOGrouping, 'none')
      subplot(a,b,r(k,j))
       %Obtains the imaginary and real outputs of the nyquist function for the system and puts them in the correct formatting
       for i = 1:length(Re(k,j,:))
           X(i) = Re(k,j,i);
       end
       for i = 1:length(Im(k,j,:))
           Y(i) = Im(k,j,i);
       end
       %Plots the imaginary and real values in a nyquist plot format
       if k==1 && j==1
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       else
       plot(X,Y,'color',linecolor);
       end
       hold on
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));
        end
        
        
        grid_options_set
        
        input_label_set
        
        output_label_set
             
    elseif strcmpi(opts.IOGrouping, 'inputs')
       subplot(a,1,c(k))
       %Obtains the imaginary and real outputs of the nyquist function for the system and puts them in the correct formatting
       for i = 1:length(Re(k,j,:))
           X(i) = Re(k,j,i);
       end
       for i = 1:length(Im(k,j,:))
           Y(i) = Im(k,j,i);
       end
       %Plots the imaginary and real values in a nyquist plot format
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));           
        end
        
        %obtains the current axis for labeling
        if k==1 && j==1
           axax=gca;
        end  
         
        grid_options_set
        
        input_label_set
        
        output_label_set
        
    elseif strcmpi(opts.IOGrouping, 'outputs')  
       subplot(1,b,j)
       %Obtains the imaginary and real outputs of the nyquist function for the system and puts them in the correct formatting
       for i = 1:length(Re(k,j,:))
           X(i) = Re(k,j,i);
       end
       for i = 1:length(Im(k,j,:))
           Y(i) = Im(k,j,i);
       end
       
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       %Plots the imaginary and real values in a nyquist plot format
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,1))           
        end
        
        %obtains the current axis for labeling
        if k==1 && j==1
           axax=gca;
        end  
        
        grid_options_set
        
        input_label_set
        
        output_label_set
    
    else %IO Grouping is 'all'
       subplot(1,1,1)
       %Obtains the imaginary and real outputs of the nyquist function for the system and puts them in the correct formatting
       for i = 1:length(Re(k,j,:))
           X(i) = Re(k,j,i);
       end
       for i = 1:length(Im(k,j,:))
           Y(i) = Im(k,j,i);
       end
       %Plots the imaginary and real values in a nyquist plot format
       sys_axis(zzzz)=plot(X,Y,'color',linecolor);
       hold on
       if strcmpi(opts.ShowFullContour, 'On')
       plot(X,-Y,'--','color',linecolor)
       hold on
       else
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,1))           
        end
        
        %obtains the current axis for labeling
        if k==1 && j==1
           axax=gca;
        end  
        
        grid_options_set
        
        input_label_set
        
        output_label_set
             
    end
       %obtains the appropriate axes for labeling purpose
       if k==1 && j==1
       axax=hAx1;
       end
       if k==a && j==b
       axaxx=hAx1;
       end    
  end
end

end


%sets the input label set for the specified plots
function input_label_set
%       Creates the input labels for the appropriate subplots
        if k == 1
          if flag==1            
           title(['From: In(', num2str(j) ,')'],'fontsize',opts.InputLabels.FontSize,'fontweight',opts.InputLabels.FontWeight,'fontangle',...
                  opts.InputLabels.FontAngle,'color',opts.InputLabels.Color,'interpreter',opts.InputLabels.Interpreter);
          end
        else
        end
end

%Sets the output labels for the subplots
function output_label_set
%       Creates output labels at appropriate subplots
        if j == 1
          if flag==1
               ylabel((['To: Out(', num2str(k) ,')']),'fontsize',opts.OutputLabels.FontSize,'fontweight',opts.OutputLabels.FontWeight,'fontangle',...
                  opts.OutputLabels.FontAngle,'color',opts.OutputLabels.Color,'interpreter',opts.OutputLabels.Interpreter);
               set(get(gca,'ylabel'),'rotation',90);
          else
          end
        else
        end
end
%responsible for turning grid on and changing its property set
function grid_options_set
           hAx1 = gca;
        if strcmpi(opts.Grid, 'off')
           grid off
        else
           grid on
                   %# get a handle to first axis

                %# create a second transparent axis, same position/extents, same ticks and labels
                hAx2 = axes('Position',get(hAx1,'Position'), ...
                   'Color','none', 'Box','on', ...
                   'XTickLabel',get(hAx1,'XTickLabel'), 'YTickLabel',get(hAx1,'YTickLabel'), ...
                   'XTick',get(hAx1,'XTick'), 'YTick',get(hAx1,'YTick'), ...
                   'XLim',get(hAx1,'XLim'), 'YLim',get(hAx1,'YLim'));

                %# show grid-lines of first axis, give them desired color, but hide text labels
                set(hAx1, 'XColor',opts.GridColor, 'YColor',opts.GridColor, ...
                   'XMinorGrid','off', 'YMinorGrid','off', ...
                   'XTickLabel',[], 'YTickLabel',[]);

                %# link the two axes to share the same limits on pan/zoom
                 linkprop([hAx1 hAx2],{'position','xscale'});

        end
end

%Sets the xlabel to have an exponential label set
function xlabel_exponential_form_set
%%  %   Makes the xlabels have exponential notation where needed       
        if strcmpi(opts.FreqScale,'log')
         xt = get(gca, 'xtick');
         get(gca,'xtick'); 
         %gets the superscript terms of the x-axis label
         x_ex = floor(log10(xt));
         x_mant = xt./10.^x_ex;
         %sets a tolerance for if the number is within the floor number
         tol=1e-10;
         xscientific = [x_mant; x_ex];
         if (x_mant-1)<tol
         set(gca, 'xticklabel', sprintf('10^{%d}|', x_ex));
         else
         set(gca, 'xticklabel', sprintf('{%1.1g}x10^{%d}|', xscientific));
         end
         if exist('OCTAVE_VERSION', 'builtin')
            set(gca,'interpreter','tex');
         end
        end   
end


end

% Creates a new figure in which axes are added to
set(gcf,'NextPlot','add');
axes;

% Creates the title, xaxis, and yaxis in accordance to the specified
% settings
h = title(opts.Title.String,'fontsize',opts.Title.FontSize,'fontweight',opts.Title.FontWeight,'fontangle',...
         opts.Title.FontAngle,'color',opts.Title.Color,'interpreter',opts.Title.Interpreter);
a = xlabel('Real Axis','fontsize',opts.XLabel.FontSize,'fontweight',opts.XLabel.FontWeight,'fontangle',...
          opts.XLabel.FontAngle,'color',opts.XLabel.Color,'interpreter',opts.XLabel.Interpreter);
b = ylabel('Imaginary Axis','fontsize',opts.YLabel.FontSize,'fontweight',opts.YLabel.FontWeight,'fontangle',...
          opts.YLabel.FontAngle,'color',opts.YLabel.Color,'interpreter',opts.YLabel.Interpreter);
% Sets the figure itself off
set(gca,'visible','off');
% Alters the positon of the title so it can be better seen
post = get(h,'Position');
post(2) = post(2)+0.03;
% Alters the positon of the xlabel so it can be better seen
posx = get(a,'Position');
posx(2) = posx(2)*1.3;
posx(3)=post(3);
% Alters the positon of the ylabel so it can be better seen
posy = get(b,'Position');
posy(1) = posy(1)*1.3;
posy(3)=post(3);
% Causes the title and axis labels to become visible and there new positons
% cause it so that they are not interfering with the subplots
set(h,'visible','on','Position',post);
set(a,'visible','on','Position',posx);
set(b,'visible','on','Position',posy);
set(get(gca, 'ylabel'),'rotation',90);
if exist('OCTAVE_VERSION', 'builtin')
%turns off the visibility of the axes if the progrm is run in octave
set(h,'visible','off','Position',post);
set(a,'visible','off','Position',posx);
set(b,'visible','off','Position',posy);
%creates labels for the plots
w=text(posy(1), posy(2),'Imaginary Axis');
z=text(posx(1), posx(2), 'Real Axis');
%rotates the ylabel and centers it on the yaxis
set(w,'rotation',90,'HorizontalAlignment','center')
%positions the ylabel on the center of yaxis of th figure
set(z,'HorizontalAlignment','center')
%rotates the ylabel appropriately for proper labeling
set(get(gca, 'ylabel'),'rotation',90)
%Sets the labels according to the specified options
set(w,'fontsize',opts.YLabel.FontSize,'fontweight',opts.YLabel.FontWeight,'fontangle',...
    opts.YLabel.FontAngle,'color',opts.YLabel.Color,'interpreter',opts.YLabel.Interpreter);
set(z,'fontsize',opts.XLabel.FontSize,'fontweight',opts.XLabel.FontWeight,'fontangle',...
    opts.XLabel.FontAngle,'color',opts.XLabel.Color,'interpreter',opts.XLabel.Interpreter);
%Sets the positon in the top and center and properties of the title according to the specified options
q=text(post(1), post(2), opts.Title.String);
set(q,'HorizontalAlignment','center')
set(q,'fontsize',opts.Title.FontSize,'fontweight',opts.Title.FontWeight,'fontangle',...
    opts.Title.FontAngle,'color',opts.Title.Color,'interpreter',opts.Title.Interpreter);
end
%creates the legend content for the plots
xx=linspace(1,qqq,qqq);
for qq=1:qqq
     yyyy(qq)=cellstr(['sys(',num2str(xx(qq)),')']);
end
%sets the legend for the system plots
leg=legend(sys_axis,yyyy);
%Repositions the legend if any of the system inputs is MIMO
if as>1 && bs>1
dummyleg=legend(axaxx,'  ');
post=get(dummyleg,'position');
set(leg,'position',post);
legend(axaxx,'off');
end

end

       


        