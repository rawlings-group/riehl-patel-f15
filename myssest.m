function [sys, g] = myssest(data, nx)
% [sys] = myssest(data, nx)
% [sys, g] = myssest(data, nx)
% 
% 

% -------------------------------------------------------------------------
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

% Read the input-output data.
if exist('OCTAVE_VERSION', 'builtin')
    % Octave incompatibility with MATLAB in iddata.
    y = data.y{1,1}';
    u = data.u{1,1}';
    Ts = data.Ts{1,1};
else
    y = data.y; 
    u = data.u;
    Ts = data.Ts;
end

% -------------------------------------------------------------------------
% Identify a black-box state-space model using UNIT.
% -------------------------------------------------------------------------
z.y = y; z.u = u; z.T = Ts;
m.op = 's';         % Continuous time.
m.type = 'ss';      
m.nx = nx;          % Specify the order.
opt.alg = 'gn';     % Use Gauss-Newton gradient based search.
opt.cost = 'trace'; % Use quadratic cost in prediction error.

g = est(z, m, opt); % Do the estimation.

sys = g.sysG;

end
