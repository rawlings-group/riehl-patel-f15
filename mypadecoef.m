function [num, den] = mypadecoef(T, N)
%   [num, den] = mypadecoef(T, N) creates a Nth order Pade approximation of the
%   time delay transfer function exp(-T*s).
%   
%   The outputs are row vectors of coefficients in descending powers of s 
%   for the numerator and denominator polynomials.
% 
%   Example:
%     [num,den] = mypadecoef(3,5)
%
%    num =
%        -1.0000   10.0000  -46.6667  124.4444 -186.6667  124.4444
% 
%    den =
%        1.0000   10.0000   46.6667  124.4444  186.6667  124.4444

% -------------------------------------------------------------------------
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

% Formula is recursive:
%
%   P(x) = \sum_{k = 0}^N p[k] x^(N - k)
%
%   p[0] = 1
%   p[k] = (N+k)*(N-k+1)/(k*T) * p[k-1]
%
%   num = P(-x)
%   den = P(x)

% Input checking.
if nargin() ~= 2
    error('Requires exactly 2 arguments!')
elseif T <= 0
    error('T must be strictly positive!')
end    
N = round(N);
if N < 0
    error('N must be nonnegative!')
end

% Initialize first coefficients.
p = zeros(1, N + 1);
p(1) = 1; % Leading coefficient is 1. factorial(2*N)/factorial(N)/T^N;

% Loop and update recursively.
for k = 1:N
    kk = k + 1; % Swap from 0-based to 1-based indexing.
    p(kk) = (N+k)*(N-k+1)/(k*T)*p(kk - 1);
end

% Numerator is p with alternating signs. Denominator is p.
num = p;
if mod(N, 2) == 0
    num(2:2:end) = -num(2:2:end);
else
    num(1:2:end) = -num(1:2:end);
end
den = p;

end

