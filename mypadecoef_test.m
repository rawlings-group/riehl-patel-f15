clear
clc
if exist('OCTAVE_VERSION', 'builtin')
    pkg load control
    pkg load signal
end

[num1,den1]=mypadecoef(3,5)

%Compare this to the MATLAB padecoef function
if ~exist('OCTAVE_VERSION', 'builtin')
[num2,den2]=padecoef(3,5)
end
