clear
clc

%   Define the graphics toolkit that is compatible with the mybode function
if exist('OCTAVE_VERSION', 'builtin')
    graphics_toolkit('fltk')
    graphics_toolkit('qt')
%    graphics_toolkit('gnuplot')
    pkg load control
    pkg load signal
end

%   Define a transfer function system for the bode plots
s = tf('s');
x1 = 1/(s+1); %Simple first order system
x2 = 1/(1+s^3+s^2); %Different first order system
x3 = (5)/(s+4); %Simple lead/lag system
x4 = 1/(s^2+2*s+1); %Critically Damped second order system
x5 = 1/(s^2+3*s+1); %Overdamped Second Order System
x6 = 1/(s^2+s+1); %Underdamped Second Order System
X = [x1 x2 x3; x4 x5 x6];
X1=[x1 x2];
X2=[x3 x4; x5 x6];
opts=mybodeoptions;
%   Octave demo using mybode function
%   Listed are available options that can be altered
opts.Title.String = 'Bode Diagram';
opts.Title.FontSize = 8;
opts.Title.FontWeight = 'Normal';
opts.Title.FontAngle = 'Normal';
opts.Title.Color = [0 1 0];
opts.Title.Interpreter = 'tex';
opts.XLabel.String = 'Frequency';
opts.XLabel.FontSize = 8;
opts.XLabel.FontWeight = 'Normal';
opts.XLabel.FontAngle = 'Normal';
opts.XLabel.Color = [0 0 1];
opts.XLabel.Interpreter = 'tex';
opts.YLabel.String = {'Magnitude'  'Phase'};
opts.YLabel.FontSize = 8;
opts.YLabel.FontWeight = 'Normal';
opts.YLabel.FontAngle = 'Normal';
opts.YLabel.Color = [0 0 0];
opts.YLabel.Interpreter = 'tex';
opts.TickLabel.FontSize = 8;
opts.TickLabel.FontWeight = 'Normal';
opts.TickLabel.FontAngle = 'Normal';
opts.TickLabel.Color = [0 0 0];
opts.Grid = 'on';
if exist('OCTAVE_VERSION', 'builtin')
opts.GridColor = [0 0 0];
end
opts.XLim = [1 10];
opts.XLimMode = {'auto'};
opts.YLim = {[1 10]};
opts.YLimMode = {'auto'};
opts.IOGrouping = 'none';
opts.InputLabels.FontSize = 8;
opts.InputLabels.FontWeight = 'Normal';
opts.InputLabels.FontAngle = 'Normal';
opts.InputLabels.Color = [0.4000 0.4000 0.4000];
opts.InputLabels.Interpreter = 'tex';
opts.OutputLabels.FontSize = 8;
opts.OutputLabels.FontWeight = 'Normal';
opts.OutputLabels.FontAngle = 'Normal';
opts.OutputLabels.Color = [0.4000 0.4000 0.4000];
opts.OutputLabels.Interpreter = 'tex';
opts.InputVisible = {'on'};
opts.OutputVisible = {'on'};
opts.FreqUnits = 'rad/s';
opts.FreqScale = 'log';
opts.MagUnits = 'dB';
opts.MagScale = 'linear';
opts.MagVisible = 'on';
opts.MagLowerLimMode = 'auto';
opts.MagLowerLim = 0;
opts.PhaseUnits = 'deg';
opts.PhaseVisible = 'on';
opts.PhaseWrapping = 'off';
opts.PhaseMatching = 'off';
opts.PhaseMatchingFreq = 5;
opts.PhaseMatchingValue = 90;
%   Feature in mybodeoptions lets the options you want to alter prior to
%   calling the mybodeoptions function unlike the bodeoptions function in
%   MATLAB
wout=linspace(-100,100,200);
mybode(X1,X2,opts);
% %  Compare the results of two separate first order SISO system
% mybode(x1,opts)
% mybode(x2,opts)

% %   Compare this result to the MATLAB bodeplot function
% if ~exist('OCTAVE_VERSION', 'builtin')
% figure
% bodeplot(x1,opts)
% end




