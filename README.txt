This repository tracks the code files from the undergraduate research
project completed by Jason Riehl working under Nishith Patel and James
Rawlings in the Fall 2015 semester at the University of Wisconsin-Madison.

The primary purpose of the research project is to write code to expand
the functionality of the Octave control toolbox and mimic the control
systems toolbox of MATLAB. To this end, the following files were 
written:

1). Bode Plot Options (bodeoptions/bodeplot)

    mybodeoptions - provides advanced plotting options for mybode
    mybode - employs mybodeoptions for making bode plots for SISO/MIMO systems
    mybode_test - test script for running these files

2). Nyquist Plot Options (nyquistoptions/nyquistplot)

    mynyquistoptions - provides advanced plotting options for mynyquist
    mynyquist - employs mynyquistoptions for making nyquist plots for SISO/MIMO systems
    mynyquist_test - test script for running these files

3). Pade Approximation (padecoef)

    mypadecoef - computes the Pade approximation for a time delay
    mypadecoef_test - test script for running these files

4). System Identification (ssest/tfest)
    
    myssest - identifies a linear black-box state-space model from data
    mytfest - identifies a linear transfer function model from data
    id_test - test script for running these files
    
    Note that these scripts are simply wrappers that call system identification
    routines from the University of Newcastle Identification Toolbox (UNIT),
    which can be obtained separately (free for academic users) from:

                        http://sigpromu.org/idtoolbox/


Any questions or comments can be sent to <nishith.patel@wisc.edu>.
