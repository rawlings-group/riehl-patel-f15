function output_opts = mynyquistoptions(input_opts)
%   options  =  mynyquistoptions()
%   options  =  mynyquistoptions(options)
%
%   Creates an option field that can be modified.
% 
%   List of options were based on the bodeoptions offered in MATLAB
%
%   What is available to alter:
%        - Title, XLabel, YLabel can alter the label, and their text style
%          and color
%        - TickLabel can also alter their text style and color
%        - Grid can be turned on or off 
%        - GridColor can change the grid color in MATLAB
%        - XLimMode and YLimMode alters whether the axes have set limits or
%          automatic limits
%        - XLim, YLim sets limits for the x and y axes
%        - IOGrouping can group either the inputs, outputs, both, or none
%        - InputLabels, OutputLabels can alter the label styles of the
%          output and input labels
%        - InputVisible, OutputVisible can alter whether a input or output
%          is visible 
%        - FreqUnits sets the units of frequency 
%        - FreqScale sets the scale of the x axes to linear or log
%        - MagUnits sets the units of the magnitude chosen from dB or abs
%        - MagScale sets the scale of the magnitude axes to linear or log
%        - MagVisible sets whether the Magnitude plot(s) are visible
%        - MagLowerLimMode sets whether the magnitude axis has a set lower
%          limit or if it is automatic, overrides YLim command
%        - MagLowerLim sets the lower limit for the magnitude axes
%        - PhaseUnits sets the units of the phase chosen from deg or rad
%        - PhaseVisible sets whether the phase plot(s) are visible
%        - PhaseWrapping enables if phase wrapping is done to the phase
%          output
%        - PhaseMatching enables if phase matching is done to the phase
%          output
%        - PhaseMatchingFreq specifies frequency used for phase matching
%        - PhaseMatchingValue specifies is the value that the phase is set
%          to be close to
%        - ConfidenceRegionNumberSD is the number of standard deviations 
%          used when displaying the confidence region characteristics for
%          identification models [NOT YET IMPLEMENTED]
% 
%   A predefined option structure can also be included as an input into the 
%   mybodeoptions function, and the unspecified options are filled in with
%   their respective default values.

% -------------------------------------------------------------------------
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

default_opts.Title.String = 'Nyquist Diagram';
default_opts.Title.FontSize = 8;
default_opts.Title.FontWeight = 'Normal';
default_opts.Title.FontAngle = 'Normal';
default_opts.Title.Color = [0 0 0];
default_opts.Title.Interpreter = 'tex';
default_opts.XLabel.String = 'Real Axis';
default_opts.XLabel.FontSize = 8;
default_opts.XLabel.FontWeight = 'Normal';
default_opts.XLabel.FontAngle = 'Normal';
default_opts.XLabel.Color = [0 0 0];
default_opts.XLabel.Interpreter = 'tex';
default_opts.YLabel.String = 'Imaginary Axis';
default_opts.YLabel.FontSize = 8;
default_opts.YLabel.FontWeight = 'Normal';
default_opts.YLabel.FontAngle = 'Normal';
default_opts.YLabel.Color = [0 0 0];
default_opts.YLabel.Interpreter = 'tex';
default_opts.TickLabel.FontSize = 8;
default_opts.TickLabel.FontWeight = 'Normal';
default_opts.TickLabel.FontAngle = 'Normal';
default_opts.TickLabel.Color = [0 0 0];
default_opts.Grid = 'off';
default_opts.GridColor = [0.1500 0.1500 0.1500];
default_opts.XLim = {[1 10]};
default_opts.XLimMode = {'auto'};
default_opts.YLim = {[1 10]};
default_opts.YLimMode = {'auto'};
default_opts.IOGrouping = 'none';
default_opts.InputLabels.FontSize = 8;
default_opts.InputLabels.FontWeight = 'Normal';
default_opts.InputLabels.FontAngle = 'Normal';
default_opts.InputLabels.Color = [0.4000 0.4000 0.4000];
default_opts.InputLabels.Interpreter = 'tex';
default_opts.OutputLabels.FontSize = 8;
default_opts.OutputLabels.FontWeight = 'Normal';
default_opts.OutputLabels.FontAngle = 'Normal';
default_opts.OutputLabels.Color = [0.4000 0.4000 0.4000];
default_opts.OutputLabels.Interpreter = 'tex';
default_opts.InputVisible = {'on'};
default_opts.OutputVisible = {'on'};
default_opts.FreqUnits = 'rad/s';
default_opts.MagUnits = 'dB';
default_opts.PhaseUnits = 'deg';
default_opts.ShowFullContour='on';
default_opts.ConfidenceRegionNumberSD = 1; % ignore for now
default_opts.ConfidenceRegionDisplaySpacing = 5; % ignore for now
%This allows for altering the option set prior to calling the mybodeoptions
%function  
if nargin > 0
%Defines the output set structure field equal to the input set structure
%field
output_opts  =  input_opts;

%This loop check fills any empty set components that were not specified in
%the input set structure field
structfields  =  fieldnames(default_opts);
n = length(structfields);
for i = 1:n
   if isstruct(default_opts.(structfields{i}));
      innerstructfields=fieldnames(default_opts.(structfields{i}));
      m=length(innerstructfields);
      for j=1:m
            innerfieldname = innerstructfields{j};
            fieldcheck = isfield(input_opts,structfields{i}) && isfield(input_opts.(structfields{i}),innerfieldname);
            if ~fieldcheck
                output_opts.(structfields{i}).(innerfieldname) = default_opts.(structfields{i}).(innerfieldname); 
            end
      end
   else
      fieldname = structfields{i};
      fieldcheck = isfield(input_opts, fieldname);
      if ~fieldcheck
         output_opts.(fieldname) = default_opts.(fieldname); 
      end
   end
end
%For if no predefined option set is included as a mybodeoption input 
% structure field
else
output_opts = default_opts;
end

end
