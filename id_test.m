% Performs a simple identification example using UNIT.

close all

% -------------------------------------------------------------------------
% ID Data Parameters. 
% -------------------------------------------------------------------------
k           = 1;      % True Scaled Heat Transfer Coefficient.
n_data      = 60; 
Ts          = 0.1; 
yNoise      = 0.005; 
dNoise      = 0.0005;
% -------------------------------------------------------------------------

% Seed for Random Number Generator.
rng(1)

% Plant model.
A = -k; B = k; C = 1;
sysd = c2d(ss(A, B, C, 0), Ts);
Ad = sysd.a; Bd = sysd.b; Cd = sysd.c;

% Perform a step test.
u = [zeros(1,5), ones(1, n_data - 5)]; 
xplant = 0;
y = zeros(1, n_data);

for i = 1:n_data
    y(:,i) = Cd*xplant + sqrt(yNoise)*randn;
    xplant = Ad*xplant + Bd*u(:,i) + sqrt(dNoise)*randn;
end

tsim = 0:Ts:((n_data-1)*Ts);

% Plot data.
h_data = figure;
subplot(2,1,1)
plot(tsim, u, '.-', 'LineWidth', 1, 'MarkerSize', 5)
ylabel('u')
set(get(gca,'YLabel'),'Rotation',0)
ylim([-0.2, 1.5])
subplot(2,1,2)
plot(tsim, y, 'k.', 'LineWidth', 1, 'MarkerSize', 5)
xlabel('time')
ylabel('y')
set(get(gca,'YLabel'),'Rotation', 0)

% Specify the input-output data.
data = iddata(y', u', Ts);

% -------------------------------------------------------------------------
% Identify a black-box state-space model using UNIT.
% -------------------------------------------------------------------------
% Specify the order.
nx = 1;
[id_ss, g_ss] = myssest(data, nx);
details(g_ss);
yid_ss = lsim(id_ss, u', tsim');

% -------------------------------------------------------------------------
% Identify a transfer function model using UNIT.
% -------------------------------------------------------------------------
% Specify the number of poles and zeros.
np = 1; nz = 0;
[id_tf, g_tf] = mytfest(data, np, nz);
details(g_tf);
yid_tf = lsim(id_tf, u', tsim');

% -------------------------------------------------------------------------
% Identify a grey-box state-space model using UNIT.
% -------------------------------------------------------------------------
z.y = y'; z.u = u'; z.T = Ts;
m_ss2.op = 's'; m_ss2.nx = 1; m_ss2.type = 'ss'; 
opt_ss2.alg = 'gn'; opt_ss2.cost = 'trace';

% Initial guess.
m_ss2.par   = 'struct';
kguess = 2;
m_ss2.ss.A = -kguess; m_ss2.ss.B = kguess; m_ss2.ss.C = 1; m_ss2.ss.D = 0;
m_ss2.ss.K = []; m_ss2.ss.F = []; m_ss2.ss.G = []; m_ss2.ss.X1 = [];

% Set structure. (1's indicate variables, 0's indicate fixed parameters)
m_ss2.ss.Ai = true; m_ss2.ss.Bi = true; 
m_ss2.ss.Ci = false; m_ss2.ss.Di = false;
m_ss2.ss.Ki = []; m_ss2.ss.Fi = []; m_ss2.ss.Gi = []; m_ss2.ss.X1i = [];

g_ss2 = est(z, m_ss2, opt_ss2); % Do the estimation.

details(g_ss2);         % Present the results.
validate(z, g_ss2);
id_ss2 = g_ss2.sysG;
yid_ss2 = lsim(id_ss2, u', tsim');

% -------------------------------------------------------------------------
% Plotting results.
% -------------------------------------------------------------------------
h_idss = figure;
plot(tsim, y, 'k.', tsim, yid_ss, 'b', 'LineWidth', 1, 'MarkerSize', 5)
xlabel('time')
ylabel('y')
legend('Data', 'ID', 'Location', 'SouthEast')
set(get(gca,'YLabel'),'Rotation', 0)

h_idss2 = figure;
plot(tsim, y, 'k.', tsim, yid_ss2, 'r', 'LineWidth', 1, 'MarkerSize', 5)
xlabel('time')
ylabel('y')
legend('Data', 'ID', 'Location', 'SouthEast')
set(get(gca,'YLabel'),'Rotation', 0)

h_idtf = figure;
plot(tsim, y, 'k.', tsim, yid_tf, 'g', 'LineWidth', 1, 'MarkerSize', 5)
xlabel('time')
ylabel('y')
legend('Data', 'ID', 'Location', 'SouthEast')
set(get(gca,'YLabel'),'Rotation', 0)

h_idall = figure;
plot(tsim, y, 'k.', tsim, yid_tf, 'g', tsim, yid_ss, 'b', tsim, yid_ss2, 'r', 'LineWidth', 1, 'MarkerSize', 5)
xlabel('time')
ylabel('y')
legend('Data', 'Black-Box TF', 'Black-Box SS', 'Grey-Box SS', 'Location', 'SouthEast')
set(get(gca,'YLabel'),'Rotation', 0)
