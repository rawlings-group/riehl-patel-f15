clear
clc
close all
s = tf('s');
x1 = 1/(s+1); %Simple first order system
x2 = 1/(1+s^3+s^2); %Different first order system
x3 = (5)/(s+4); %Simple lead/lag system
x4 = 1/(s^2+2*s+1); %Critically Damped second order system
x5 = 1/(s^2+3*s+1); %Overdamped Second Order System
x6 = 1/(s^2+s+1); %Underdamped Second Order System
X = [x1 x2 x3; x4 x5 x6];
X1=[x1 x2];
X2=[x3 x4; x5 x6];
opts=mynyquistoptions;
% opts.OutputVisible={'on' 'on'};
opts.IOGrouping='none';
mynyquist(X1,X2,opts)

% MATLAB for com
if ~exist('OCTAVE_VERSION', 'builtin')
    mat_opts=nyquistoptions;
    mat_opts.IOGrouping='none';
    figure
    nyquistplot(X1,X2,mat_opts)
end