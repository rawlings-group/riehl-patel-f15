function mybode(varargin)
%
% 
%   mybode(sys1,sys2,...)
%   mybode(sys1,sys2,..., opts)
%   mybode(sys1,sys2,...,wout,opts) 
%   mybode(sys1,sys2,...,wout)
%    
%   Where sys is the system input, wout is the output frequency range, and opts 
%   is a modifiable option set 
%    
%   Bode diagram generator with plot modification capabilities offered in 
%   MATLAB.
%
%   sys can be a matrix of or single transfer function inputs  
% 
%   Relies on an option structure from mybodeoptions. See mybodeoptions for
%   documentation on the various options.
%
%   The three variables consistent in this program are the magnitude (Mag or mag),
%   phase (Phase or phase), and the output frequency (wout).
% 
%   For SISO system with default options, the normal bode function is 
%   recommended.
% 
%   Example:
%   sys = tf(1, [1 1]);
%   opts = mybodeoptions;
%   % Changes title color from default black to green   
%   opts.Title.Color = [0 1 0];
%   mybode(sys, opts) 

% -------------------------------------------------------------------------
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------



% Creates an initial figure for the plot(s)
figure
%Creates a color set for having different systems having different plot colors
ycolors=get(gca,'colororder');
%Creates a standard output frequency range
wout={1e-2 100};
%Check to see what input type was specified
%if an option set is specified
if isstruct(varargin{end})
   opts  =  varargin{end};
   %if a frequency set is specified
   if isa(varargin{end-1},'numeric')
      wout  =  varargin{end-1};
      %gets the number of system inputs
      qqq=nargin-2;
      %rest of inputs are the transfer fuction systems
       aa = varargin(1:(nargin-2));
   else %if no output frequency range is given
      %gets the number of system inputs
      qqq=nargin-1;
      %rest of inputs are the transfer fuction systems
      aa = varargin(1:(nargin-1));
   end
% if no option set is inputted but a output frequency range is
elseif isa(varargin{end},'numeric')
%  obtains the standard bode option set
   opts=mybodeoptions;
   wout = varargin{end};
   %get the number of system inputs
   qqq=nargin-1;
   %rest of inputs are the transfer fuction systems
   aa = varargin(1:(nargin-1));
 
else %If the input does not specify an option set or an output frequency
%  obtains the standard bode option set
  opts=mybodeoptions;
  %get the number of system inputs
  qqq=nargin;
  %rest of inputs are the transfer fuction systems
  aa = varargin(1:(nargin));
end
%resizes the size of the system inputs according to visibility specifications
for iii = 1:length(aa)  
       %gets size of given system input
       y  =  ones(size(aa{iii}));
       [mmm(iii),nnn(iii)]  =  size(y);
       %obtains the max size of the system inputs
       aaaaa=max(mmm);
       bbbbb=max(nnn);      
       %function resizes the size matrix if specified
       x=resize_bode(y);
       [mm(iii),nn(iii)]  =  size(x);
       %obtains new max size of the system inputs after resizing 
       as=max(mm);
       bs=max(nn);
end 
%sets initial value for flag for proper evaluation
flag=0;

for iiii = 1:length(aa)
    %obtains given system input
    aaa=aa{iiii};
    %gets appropriate linecolor for system input
    linecolor=ycolors(iiii,:);
    %check to see if the given system is the max size system for labeling purposes
    if flag==1
       flag=0;
    elseif isequal([mmm(iiii) nnn(iiii)],[aaaaa bbbbb])
       flag=1;
    else
       flag=0;
    end  
    %bode function that creates the plots
    bode_figure(aaa,opts,as,bs,flag,linecolor,wout)
    hold on
end

%Function to resize the size matrix for the subplot size specifications
function x=resize_bode(y)
       %gets size of given system input
       [m,n]  =  size(y);
       %Makes an input cell array of the correct size for rest of function
       if strcmpi(opts.InputVisible,'on')
          opts.InputVisible  =  {};
          for i = 1:n
              opts.InputVisible(i) = {'on'};
          end
       else
       end
       %Makes an output cell array of the correct size for rest of function      
       if strcmpi(opts.OutputVisible,'on')
          opts.OutputVisible = {};
          for ii = 1:m
              opts.OutputVisible(ii) = {'on'};
          end
       else
       end
       %these two loops changes the size of the size matrix if specified
       for j = 1:n
           if strcmpi(opts.InputVisible(j),'off')
              y(:,j)=[];    
           else
           end
       end
       
       for k = 1:m
           if strcmpi(opts.OutputVisible(k),'off')
              y(k,:)=[];
           else
           end
       end
%sets the new size of the size matrix
x=y;
       
end


function [] = bode_figure(sys_input, opts, maxrows, maxcols, flag, linecolor,WOUT)
%Size fucntion used to Obtain Size of transfer function matrix to create 
%coordinate system for subplots
a=maxrows;
b=maxcols;
%gets size of current system being plotted
y  =  ones(size(sys_input));
[m,n]  =  size(y);     
%Makes an input cell array of the correct size for rest of function   
if strcmpi(opts.InputVisible,'on')
    opts.InputVisible  =  {};
    for i = 1:n
      opts.InputVisible(i) = {'on'};
    end
else
end
%Makes an output cell array of the correct size for rest of function 
if strcmpi(opts.OutputVisible,'on')
    opts.OutputVisible = {};
    for ii = 1:m
      opts.OutputVisible(ii) = {'on'};
    end
else
end

%Used to obtain values for Bode plot fo each input/output pairing
for k = 1:m 
  for j = 1:n
%    Alters length of X-axis to comply with the set XLim
     if strcmpi(opts.XLimMode,'manual')
        [mag,phase] = bode(sys_input(k,j),opts.XLim);
        if strcmpi(opts.MagUnits, 'dB')
        mag = mag2db(mag);
        end
         wout = opts.XLim;
         for i = 1:length(mag)
             Mag(k,j,i) = mag(i);    
         end
         for h = 1:length(phase)
             Phase(k,j,h) = phase(h);    
         end
     else  %if no XLim is specified and the inputted output frequency is used
        [mag,phase,wout] = bode(sys_input(k,j),WOUT);
        %specifies the units of the Magnitude
        if strcmpi(opts.MagUnits, 'dB')
        mag = mag2db(mag);
        end
        %two for loops put phase and mag outputs in appropriate matrix form
         for i = 1:length(mag)
             Mag(k,j,i) = mag(i);    
         end
         for h = 1:length(phase)
             Phase(k,j,h) = phase(h);    
         end
     end
%     Default units of Bode function for magnitude are absolute, this changes it 
%     to decibals if the user specifies it
   end
end
%Removes the specified inputs from the bode function data
for j = 1:n
    if strcmpi(opts.InputVisible(j),'off')
       Mag(:,j,:) = [];
       Phase(:,j,:) = [];      
    else
    end
end
%Removes the specified outputs from the bode function data
for k = 1:m
    if strcmpi(opts.OutputVisible(k),'off')
       Mag(k,:,:) = [];
       Phase(k,:,:) = [];  
    else
    end
end
%Obtains the new size of the transfer function after possible deletion of 
%inputs/outputs from the transfer function
y = ones((size(Mag(:,:,1))));
[m,n] = size(y);


%Turns the cell input for the specified YLim's and turns them into matrix 
%inputs
if strcmpi(opts.YLimMode,'manual')
    for i = 1:length(opts.YLim)
      YLim(:,:,i) = cell2mat(opts.YLim(i));
    end
else
end

%The following four for loops obtain size matrices for coordinates of the plots being used
for k = 1:m
  if k == 1
     c(k) = 1;
  else
     c(k) = c(k-1)+2;
  end
end
for k = 1:m 
  for j = 1:n
      if j == 1 && k == 1
         r(k,j) = y(k,j);
         p = r(k,j);
      elseif j == 1 
         r(k,j) = p+n+1;
         p = r(k,j);
      else
         r(k,j) = p+1;
         p = r(k,j);
      end
  end
end
for k = 1:2*m 
  for j = 1:n
      if j == 1 && k == 1
         q(k,j) = y(k,j);
         p = q(k,j);
      else
         q(k,j) = p+1;
         p = q(k,j);
      end
  end
end
for k = 1:m 
  for j = 1:n
      if j == 1 && k == 1
         s(k,j) = y(k,j);
         p = s(k,j);
      else
         s(k,j) = p+1;
         p = s(k,j);
      end
  end
end

for k = 1:m 
  for j = 1:n
%Where the plotting of the bode plots began
if strcmpi(opts.IOGrouping, 'none')
     if strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'on');
%      Plots Magnitude output against frequency output 
       hold on
       subplot(2*a,b,r(k,j))
%      Alters the Magnitude output values so that it can be plotted against
%      frequency output
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end
%      Size agreement issues cause it that the last vlaue of the Magintude 
%      output is dropped so that it can be plotted against output frequency
       plot(wout,Z(1:length(wout)),'color',linecolor)
       hold on
       
       magnitude_plot_axes_scale_set
        
       magnitude_ylim_set
       
       grid_options_set
%      Alters the subplots ticks and their labels to specified settings
            set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)  
         
       xlabel_exponential_form_set
       
       input_label_set
       
       output_label_set
%      Plots phase output against frequency output
       hold on
       
       subplot(2*a,b,(r(k,j)+b))
%       Condition causes Phase Wrapping to be done to phase output
        phase_wrapper_set
%       Condition causes Phase Matching to be done to phase output
        phase_matching_set
%       Alters the phase output values so that they can be plotted against 
%       the output frequnecy
        for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
        end 
%       Size agreement issues cause it that the last value of the Phase
%       output is dropped so that it can be plotted against the frequency
%       output
        plot(wout,Z(1:length(wout)),'color',linecolor)
        
        hold on
        
        set(gca,'xscale',opts.FreqScale)
%       Sets the YLim of the Y-axes for the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)+1))
        end
        
        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Creates output labels at appropriate subplots
        output_label_set
    
%    For when Magnitude outputs are visible and Phase outputs are not 
     elseif strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'off');
       subplot(a,b,q(k,j))
%      Alters the Magntidue output values so that they be plotted against
%      the frequency output
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end       
%      Size agreement issues cause that the last magnitude output has 
%      to be dropped so that it can be plotted against output frequency
       plot(wout,Z(1:length(wout)),'color',linecolor)        
       hold on
       
       magnitude_plot_axes_scale_set
       %Changes the limit of the x-axis if specified
       if strcmpi(opts.XLimMode,'manual')
           xlim(opts.XLim);
       end
       
       %Sets the YLim of the Y-axes in the current row of subplots
        magnitude_ylim_set
        
        grid_options_set

%       Sets the subplots ticks and thier labels to their specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set

        input_label_set
        
        output_label_set
        
      else %for when phase is visible and mag is not visible 
%       Condition causes Phase Wrapping to be done to the phase output
        phase_wrapper_set
%       Condition causes Phase Matching to be done to the phase output       
        phase_matching_set
%      Plots the phase output against frequency output 
       subplot(a,b,q(k,j))
%      Alters phase output values so that it can be plotted against output
%      frequency 
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end  
%      Size agreement issues cause it that the last phase value is dropped
%      so that it can be plotted against frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)  
       hold on
%       Sets scale of all the X-axes
        set(gca,'xscale',opts.FreqScale)
%       Sets XLim of all the X-axes
        if strcmpi(opts.XLimMode, 'manual')
           xlim(opts.XLim)
        else
        end
        
%       Sets YLim of the Y-axes of the current row of subplots 
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,k))
        else
        end
        
        grid_options_set
        
%       Sets subplots ticks and thier labels to specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Creates input labels for appropriate subplots
        input_label_set
        
        out_label_set
         
     end
        
%  Condition for if the all inputs are to be grouped together
   elseif strcmpi(opts.IOGrouping, 'inputs')
%    For when both the magnitude and phase diagrams are both set to be
%    visible
     if strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'on');
       subplot(2*a,1,c(k))
%    Alters the Magnitude matrix inputs so that it can be plotted against
%    the output frequency
        for i = 1:length(Mag(k,j,:))
            Z(i) = Mag(k,j,i);
        end 
%      Size agreement issues causes that the last Magnitude output value 
%      needed to be dropped so it can plotted against the output frequency
       plot(wout,Z(1:length(wout)),'color',linecolor)   
       
       hold on

        magnitude_plot_axes_scale_set
        
        magnitude_ylim_set
        
        grid_options_set
        
%       Sets subplots ticks and their labels to specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Creates output labels for appropriate subplots
        output_label_set
%       Plots phase output against frequency output
        subplot(2*a,1,c(k)+1)
%       Condition causes Phase Wrapping to be done to the phase output
        phase_wrapper_set
%       Condition causes Phase Matching to be done to the phase output
        phase_matching_set
%      Alters phase output values so that they can be plotted against the 
%      frequency output
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end   
%      Size agreement issues cause that the last Phase output value has to
%      dropped so it can be plotted against the output frequency
       plot(wout,Z(1:length(wout)),'color',linecolor)
       hold on
%       Sets the scale of all the X-axes
        set(gca,'xscale',opts.FreqScale)
%       Sets the YLim of the Y-axes in the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
        ylim(YLim(:,:,c(k)+1))
        else
        end
        
        grid_options_set
       
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Sets the output labels for the appropriate subplots
        output_label_set
%    For when the magnitude output is visible and the phase output is not

     elseif strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'off');
       subplot(a,1,k)
%      Alters magnitude matix value so that it can be plotted against the
%      output frequency
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end        
%      Size agreement issues causes the last value of the magnitude output 
%      to need to be dropped in order to be able to plot against output
%      frequency
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on

        magnitude_plot_axes_scale_set
        
%       Sets the XLim of all of the X-axes 
        if strcmpi(opts.XLimMode,'manual')
           xlim(opts.XLim);
        else
        end
        
        magnitude_ylim_set
        
        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Sets the output labels for the appropriate subplots
        output_label_set
        
     else %for when phase is visible and mag is not visible 
%       Condition casues Phase Wrapping to be done to the phase output
        phase_wrapper_set
%       Condition causes Phase Matching to be done to the phase output
        phase_matching_set
        
        subplot(a,1,k)
        
        %Alters the phase matrix value so that it can be plotted
%      against the output frequency       
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end 
%      Size agreement issues cause that the last value of the Phase output
%      matrix be dropped so it can plotted agianst the frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on
%       Sets the scale of the X-axes, chosen from linear or log
        set(gca,'xscale',opts.FreqScale)
%       Sets the XLim of the X-axes to the speicified range
        if strcmpi(opts.XLimMode, 'manual')
           xlim(opts.XLim)
        else
        end
%       Sets the YLim of the speficied row of Y-axes to the specified range
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,k))
        else
        end
        
        grid_options_set
        
%       Sets the plots ticks and their labels to the speicified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Creates output labels for the appropriate subplots
        output_label_set
        
     end 
     
%  For when the outputs of the system are specified to be grouped together
   elseif strcmpi(opts.IOGrouping, 'outputs')  
%    For when both the plots of Magnitude and Phase outputs are visible
     if strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'on');
%      Plots Magnitude output against frequency output
       subplot(2,b,j)
%      Alters the Magnitude matrix value so that it can be plotted
%      against the output frequency
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end        
%      Size agreement issues cause that the last value of the Magnitude
%      output needs to be dropped so that it can plotted against output
%      frequency
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on

        magnitude_plot_axes_scale_set     
     
        magnitude_ylim_set
        
        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings 
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
        
        input_label_set
        
%       Plots Phase output against frequency output  
        
%       Plots Phase output against frequency output       
        subplot(2,b,j+n)
%       Condition causes Phase Wrapping be done to the phase output values
        phase_wrapper_set
%       Condition causes Phase Mathcing be done to the phase output values
        phase_matching_set
%      Alters the Phase output values so that it can be plotted against
%      the output frequency 
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end  
%      Size agreement causes it that the last value of the phase output
%      needs to be dropped so it can be plotted against frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)
       
        hold on
%       Sets the scale of all the X-axes
        set(gca,'xscale',opts.FreqScale)
%       Sets the YLim mode for the specified row of subplots  
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,2))
        end
        
        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set        
        
%    For when the magnitude plots are visible and the phase plots are not
     elseif strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'off');
       subplot(1,b,j)
%      Alters the Magnitude output values so that they can be plotted
%      against the frequency output
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end        
%      Size agreement issues cause it that the last value of the Magintude
%      needs to be dropped so that it can be plotted against frequency
%      output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on

        magnitude_plot_axes_scale_set
        
%       Sets the XLim of all the X-axes, set to specified setting
        if strcmpi(opts.XLimMode,'manual')
           xlim(opts.XLim);
        else
        end
        
        magnitude_ylim_set

        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Creates input labels for the appropirate subplots
        input_label_set
                
     else %for when phase is visible and mag is not visible
%       Condition causes Phase Wrapping be done to the phase output values    
        phase_wrapper_set
%       Condition causes Phase Mathcing be done to the phase output values
        phase_matching_set

       subplot(1,b,j)
%      Alters the phase output matrix so that it can be plotted against 
%      frequency output
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end        
%      Size agreement issues cause it that the last phase value needs to be
%      dropped in order to be able to be plotted against frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on
%       Sets scales of all X-axes, chosen from linear or log
        set(gca,'xscale',opts.FreqScale)
%       Sets XLim of all X-axes
        if strcmpi(opts.XLimMode, 'manual')
           xlim(opts.XLim)
        else
        end
%       Sets YLim of the Y-axes for current row
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,1))
        else
        end
        
        grid_options_set
        
%       Sets current subplots ticks and their labels to specified settings 
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
%       Creates input labels for appropriate graphs
        input_label_set
     end          
     
   else %for when strcmpi(opts.IOGrouping, 'all')
%    For when both MAgnitude and Phase output plots are visible
     if strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'on');
       subplot(2,1,1)
%      Alters the Magnitude output values so that they can be plotted
%      against the output frequency
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end  
%      Size agreement issues cause it that the last value of the Magnitude
%      output is dropped so that it can be plotted against frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on
        
        magnitude_plot_axes_scale_set
%       Sets the YLim of the current row

        magnitude_ylim_set
        
        grid_options_set
%       Sets the subplots ticks and tick labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
        
        subplot(2,1,2)
%       Condition causes Phase wrapping to be done on the Phase output
        phase_wrapper_set
%       Condition causes Phase Matching to be done on the Phase output
        phase_matching_set
%      Alters the phase output values so that they can be plotted against 
%      output frequency 
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end        
%      Size agreement issues causes it that the last phase value is dropped
%      so that it can be plotted against frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on
%       Sets scale of all the X-axes
        set(gca,'xscale',opts.FreqScale)
%       Sets the YLim of the Y-axes of the current row
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,2))
        else
        end
       
        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
        
%    For when Magnitude plots are visible but Phase plots are not
     elseif strcmpi(opts.MagVisible, 'on') && strcmpi(opts.PhaseVisible, 'off');
%      Alters the magnitude output values so that they can be plotted
%      against the frequency output
       for i = 1:length(Mag(k,j,:))
           Z(i) = Mag(k,j,i);
       end        
%      Size agreement causes it that the last value of the magnitude
%      output is dropped so it can be plotted against frequency output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on

        magnitude_plot_axes_scale_set
        
%       Sets the XLim of all the X-axes
        if strcmpi(opts.XLimMode,'manual')
           xlim(opts.XLim);
        else
        end
        
        magnitude_ylim_set
        
        grid_options_set
        
%       Sets the subplots ticks and their labels to the specified settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
     else %for when phase is visible and mag is not visible
%       Condition causes Phase wrapping to be done to phase outputs
        phase_wrapper_set
%       Condition causes Phase mathcing to be done to the pahse outputs
        phase_matching_set
%      Alters the phase output values so that it can be plotted against the
%      frequnecy output
       for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
       end   
%      Size agreement issues cause it so that the last phase output value
%      is dropped so that it can be plotted against frequnecy output
       plot(wout,Z(1:length(wout)),'color',linecolor)
        hold on
%       Sets the scale of all the X-axes
        set(gca,'xscale',opts.FreqScale)
%       Sets the XLim of all the X-axes
        if strcmpi(opts.XLimMode, 'manual')
           xlim(opts.XLim)
        else
        end
%       Sets the YLim of the Y-axes of the current row of subplots
        if strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,1))
        else
        end
        
        grid_options_set
        
%       Sets the subplots ticks and their tick labels to th specified
%       settings
        set(gca,'FontSize', opts.TickLabel.FontSize,'FontWeight',opts.TickLabel.FontWeight,'FontAngle',...
            opts.TickLabel.FontAngle,'ycolor',opts.TickLabel.Color,'xcolor',opts.TickLabel.Color)
%%  %   Makes the xlabels have exponential notation where needed
        xlabel_exponential_form_set
     end      
    end 
       %obtains the appropriate axes for labeling purpose
       if k==1 && j==1
       axax=hAx1;
       end
       if k==b && j==a
       axaxx=hAx1;
       end        
   end  
end       
       
        
        
        
        
%       Sets the YLim of the Y-axes in the current row of subplots if specified
function magnitude_ylim_set
        if strcmpi(opts.MagLowerLimMode, 'auto') && strcmpi(opts.YLimMode, 'manual')
           ylim(YLim(:,:,c(k)));
        elseif strcmpi(opts.MagLowerLimMode, 'manual') && strcmpi(opts.YLimMode, 'manual')
           ylim([opts.MagLowerLim YLim(:,2,c(k))]);
        elseif strcmpi(opts.MagLowerLimMode, 'manual') && strcmpi(opts.YLimMode, 'auto')
           ylim([opts.MagLowerLim inf]);
        end    
end


%sets the input label set for the specified plots
function input_label_set
%       Creates the input labels for the appropriate subplots
        if k == 1
          %only applicable to largest size sys input to ensure proper labeling
          if flag==1            
           title(['From: In(', num2str(j) ,')'],'fontsize',opts.InputLabels.FontSize,'fontweight',opts.InputLabels.FontWeight,'fontangle',...
                  opts.InputLabels.FontAngle,'color',opts.InputLabels.Color,'interpreter',opts.InputLabels.Interpreter);
          end
        end
end

%Sets the output labels for the subplots
function output_label_set
%       Creates output labels at appropriate subplots
        if j == 1
          %only applicable to largest size sys input to ensure proper labeling       
          if flag==1
               ylabel((['To: Out(', num2str(k) ,')']),'fontsize',opts.OutputLabels.FontSize,'fontweight',opts.OutputLabels.FontWeight,'fontangle',...
                  opts.OutputLabels.FontAngle,'color',opts.OutputLabels.Color,'interpreter',opts.OutputLabels.Interpreter);
               set(get(gca,'ylabel'),'rotation',90);
          end
        end
end
%responsible for turning grid on and changing its property set
function grid_options_set
           %gets current axis to obtain the axis properites
           hAx1 = gca;
        if strcmpi(opts.Grid, 'off')
           grid off
        else
           grid on
                %# get a handle to first axis

                %# create a second transparent axis, same position/extents, same ticks and labels
                hAx2 = axes('Position',get(hAx1,'Position'), ...
                   'Color','none', 'Box','on', ...
                   'XTickLabel',get(hAx1,'XTickLabel'), 'YTickLabel',get(hAx1,'YTickLabel'), ...
                   'XTick',get(hAx1,'XTick'), 'YTick',get(hAx1,'YTick'), ...
                   'XLim',get(hAx1,'XLim'), 'YLim',get(hAx1,'YLim'));

                %# show grid-lines of first axis, give them desired color, but hide text labels
                set(hAx1, 'XColor',opts.GridColor, 'YColor',opts.GridColor, ...
                   'XMinorGrid','off', 'YMinorGrid','off', ...
                   'XTickLabel',[], 'YTickLabel',[]);

                %# link the two axes to share the same limits on pan/zoom
                 linkprop([hAx1 hAx2],{'position','xscale'});

        end
end

%Sets the xlabel to have an exponential label set
function xlabel_exponential_form_set
%%  %   Makes the xlabels have exponential notation where needed       
        if strcmpi(opts.FreqScale,'log')
         xt = get(gca, 'xtick');
         get(gca,'xtick'); 
         %gets the superscript terms of the x-axis label
         x_ex = floor(log10(xt));
         x_mant = xt./10.^x_ex;
         %sets a tolerance for if the number is within the floor number
         tol=1e-10;
         xscientific = [x_mant; x_ex];
         if (x_mant-1)<tol
         set(gca, 'xticklabel', sprintf('10^{%d}|', x_ex));
         else
         set(gca, 'xticklabel', sprintf('{%1.1g}x10^{%d}|', xscientific));
         end
         if exist('OCTAVE_VERSION', 'builtin')
            set(gca,'interpreter','tex');
         end
        end   
end

%sets the magnitude plot axis scale
function magnitude_plot_axes_scale_set

%       Sets scale of all the X-axes
        set(gca,'xscale',opts.FreqScale);
%       Sets scale of all the Y-axes
        if strcmpi(opts.MagUnits, 'abs') && strcmpi(opts.MagScale, 'log')
           set(gca,'yscale',opts.MagScale);
        else
           set(gca,'yscale','linear');
        end
end

%Does phase matching for the given input if specified
function phase_wrapper_set
        if strcmpi(opts.PhaseWrapping, 'on')
           for i = 1:length(Phase(k,j,:))
               if Phase(k,j,i)<-180
                  Phase(k,j,i) = Phase(k,j,i)+360;
               elseif Phase(k,j,i)>180
                  Phase(k,j,i) = Phase(k,j,i)-360;
               else
               end
           end   
        else
        end 
end

%Does hase wrapping for the given input if specified
function phase_matching_set
        if strcmpi(opts.PhaseMatching, 'on')
%         Alters the phase output values so that they can be interpolated
%         along with the frequency output
          for i = 1:length(Phase(k,j,:))
           Z(i) = Phase(k,j,i);
          end
%          Size agreement issues cause it that the last phase output value
%          is dropped so it can be interpolated along with the freeuncy
%          output
           phase_true = interp1(wout,Z(1:length(wout)),opts.PhaseMatchingFreq);
           N = round((opts.PhaseMatchingValue-phase_true)/360);
          for i = 1:length(Phase(k,j,:))
            Phase(k,j,i) = Phase(k,j,i)+N*360;
          end
        else
        end              
end



end

% Creates a new figure in which axes are added to
set(gcf,'NextPlot','add');
axes;

% Creates the title, xaxis, and yaxis in accordance to the specified
% settings
h = title(opts.Title.String,'fontsize',opts.Title.FontSize,'fontweight',opts.Title.FontWeight,'fontangle',...
         opts.Title.FontAngle,'color',opts.Title.Color,'interpreter',opts.Title.Interpreter);
a = xlabel(['Frequency (', opts.FreqUnits,')'],'fontsize',opts.XLabel.FontSize,'fontweight',opts.XLabel.FontWeight,'fontangle',...
          opts.XLabel.FontAngle,'color',opts.XLabel.Color,'interpreter',opts.XLabel.Interpreter);
b = ylabel(['Phase (', opts.PhaseUnits,'); Magnitude (', opts.MagUnits,')'],'fontsize',opts.YLabel.FontSize,'fontweight',opts.YLabel.FontWeight,'fontangle',...
          opts.YLabel.FontAngle,'color',opts.YLabel.Color,'interpreter',opts.YLabel.Interpreter);
% Sets the figure itself off
set(gca,'visible','off');
% Alters the positon of the title so it can be better seen
post = get(h,'Position');
post(2) = post(2)+0.03;
% Alters the positon of the xlabel so it can be better seen
posx = get(a,'Position');
posx(2) = posx(2)*1.3;
posx(3)=post(3);
% Alters the positon of the ylabel so it can be better seen
posy = get(b,'Position');
posy(1) = posy(1)*1.3;
posy(3)=post(3);
% Causes the title and axis labels to become visible and there new positons
% cause it so that they are not interfering with the subplots
set(h,'visible','on','Position',post);
set(a,'visible','on','Position',posx);
set(b,'visible','on','Position',posy);
set(get(gca, 'ylabel'),'rotation',90)
if exist('OCTAVE_VERSION', 'builtin')
%turns off the visibility of the axes if the progrm is run in octave
set(h,'visible','off','Position',post);
set(a,'visible','off','Position',posx);
set(b,'visible','off','Position',posy);
%creates labels for the plots
w=text(posy(1), posy(2), ['Phase (', opts.PhaseUnits,'); Magnitude (', opts.MagUnits,')']);
z=text(posx(1), posx(2), ['Frequency (', opts.FreqUnits,')']);
%rotates the ylabel and centers it on the yaxis
set(w,'rotation',90,'HorizontalAlignment','center')
%positions the ylabel on the center of yaxis of th figure
set(z,'HorizontalAlignment','center')
%rotates the ylabel appropriately for proper labeling
set(get(gca, 'ylabel'),'rotation',90)
%Sets the labels according to the specified options
set(w,'fontsize',opts.YLabel.FontSize,'fontweight',opts.YLabel.FontWeight,'fontangle',...
    opts.YLabel.FontAngle,'color',opts.YLabel.Color,'interpreter',opts.YLabel.Interpreter);
set(z,'fontsize',opts.XLabel.FontSize,'fontweight',opts.XLabel.FontWeight,'fontangle',...
    opts.XLabel.FontAngle,'color',opts.XLabel.Color,'interpreter',opts.XLabel.Interpreter);
%Sets the positon in the top and center and properties of the title according to the specified options
q=text(post(1), post(2), opts.Title.String);
set(q,'HorizontalAlignment','center')
set(q,'fontsize',opts.Title.FontSize,'fontweight',opts.Title.FontWeight,'fontangle',...
    opts.Title.FontAngle,'color',opts.Title.Color,'interpreter',opts.Title.Interpreter);
end
%creates the legend content for the plots
xx=linspace(1,qqq,qqq);
for qq=1:qqq
     yyyy(qq)=cellstr(['sys(',num2str(xx(qq)),')']);
end
%sets the legend for the system plots
leg=legend(axax,yyyy);
%Repositions the legend if any of the system inputs is MIMO
if as>1 && bs>1
dummyleg=legend(axaxx,'  ');
post=get(dummyleg,'position');
set(leg,'position',post);
legend(axaxx,'off');
end

end